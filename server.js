const express = require('express');
const bodyParser = require('body-parser');

var morgan = require('morgan');
const app = express();

//Parse App/Json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('dev'));

//Call Routes
var routes = require('./routes');
routes(app);

//Daftarkan menu routes dari index
app.use('/auth', require('./middleware'));

app.listen(8000, () => {
    console.log(`Server berjalan pada port 8000`);
});