'use strict';

var response = require('./res');
var connection = require('./connect');

exports.index = function(req,res){
    response.ok('RESTApi is Ok', res);
};

//Show Data Barang
exports.databarang = function(req, res){
    connection.query('SELECT * FROM barang', function(error, rows, fields){
        if(error){
            console.log(error); 
        }else{
            response.ok(rows, res);
        }
    });
};

// Show Data Berdasarkan Id
exports.barangbyid = function(req, res){
    let id = req.params.id;
    connection.query('SELECT * FROM barang WHERE id_barang = ?', [id], 
        function(error, rows, fields){
            if(error){
                console.log(error);
            }else{
                response.ok(rows, res);
            }
        });
};

// Create Data
exports.tambahdata = function(req, res){
    var merk = req.body.merk;
    var nama_barang = req.body.nama_barang;
    var asal = req.body.asal;

    connection.query('INSERT INTO barang (merk, nama_barang, asal) VALUES (?,?,?)',
    [merk,nama_barang,asal],
        function(error, rows, fields){
            if(error){
                console.log(error);
            }else{
                response.ok("Success insert barang", res);
            }
        });
};

//Update Data
exports.updatedata = function(req, res){
    var id_barang = req.body.id_barang;
    var merk = req.body.merk;
    var nama_barang = req.body.nama_barang;
    var asal = req.body.asal;

    connection.query('UPDATE barang SET merk=?, nama_barang=?, asal=? WHERE id_barang=?', [merk,nama_barang,asal,id_barang], 
        function(error, rows, fields){
            if(error){
                console.log(error); 
            }else{
                response.ok("Berhasil update data barang", res);
            }
        });
};

//Delete Data
exports.deletedata = function(req, res){
    var id_barang = req.body.id_barang;

    connection.query('DELETE FROM barang WHERE id_barang=?', [id_barang], 
        function(error, rows, fields){
            if(error){
                console.log(error); 
            }else{
                response.ok("Berhasil hapus data", res);
            }
        });
};

//Show bahan group
exports.groupbahan = function(req, res){
    connection.query('SELECT barang.id_barang, barang.merk, barang.nama_barang, bahan.bahan from pemesanan JOIN barang JOIN bahan WHERE pemesanan.id_barang = barang.id_barang AND pemesanan.id_bahan = bahan.id_bahan ORDER BY barang.id_barang',
        function(error, rows, fields){
            if(error){
                console.log(error);
            }else{
                response.oknested(rows, res);
            }
        }
    )
}