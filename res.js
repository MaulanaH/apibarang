'use strict';

exports.ok = function(values, res) {
    var data = {
        'status':200,
        'values':values
    };

     res.json(data);
     res.end();
};

//Nested JSON
exports.oknested = function(values, res){
    //Akumulasi
    const hasil = values.reduce((akumulasi, item)=>{
        //Key Group
        if(akumulasi[item.nama_barang]){
            //Variabel group nama barang
            const group = akumulasi[item.nama_barang];
            //Cek isi array
            if(Array.isArray(group.bahan)){
                //Tambah value ke dalam group array
                group.bahan.push(item.bahan);
            }else{
                group.bahan = [group.bahan, item.bahan];
            }
        }else{
            akumulasi[item.nama_barang] = item;
        }       
        return akumulasi;
    }, {});

    var data = {
        'status':200,
        'values':hasil
    };

     res.json(data);
     res.end();
};