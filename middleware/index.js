var express = require('express');
var auth = require('./auth');
const verifikasi = require('./verifikasi');
var router = express.Router();

//Mendaftarkan menu register
router.post('/api/v1/register', auth.register);
router.post('/api/v1/login', auth.login);

//Halaman yang butuh authorization
router.get('/api/v1/rahasia', verifikasi(2), auth.halamanrahasia);

module.exports = router;