'use strict';

module.exports = function(app) {
    var myjson = require('./controller');

    app.route('/')
        .get(myjson.index);

    app.route('/databarang')
        .get(myjson.databarang);

    app.route('/barangbyid/:id')
        .get(myjson.barangbyid);

    app.route('/tambahbarang')
        .post(myjson.tambahdata);

    app.route('/updatebarang')
        .put(myjson.updatedata);
    
    app.route('/deletebarang')
        .delete(myjson.deletedata);

    app.route('/databahan')
        .get(myjson.groupbahan);
}